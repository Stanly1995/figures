﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FiguresLibrary;

namespace MyFigures
{
    class MainClass
    {
        static void Main(string[] args)
        {
            Round round = new Round(40, 30, 25);
            round.Draw();
            Console.ReadKey();
        }
    }
}
