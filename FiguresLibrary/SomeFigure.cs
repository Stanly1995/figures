﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FiguresLibrary
{
    public class SomeFigure
    {
        #region fields
        protected int _x =0;
        protected int _y =0;
        protected int _width = 0;
        protected int _height = 0;
        protected int _radius = 0;
        #endregion;

        #region properties
        public int x
        {
            get { return _x; }
        }
        public int y
        {
            get { return _y; }
        }
        public int width
        {
            get { return _width; }
        }

        public int height
        {
            get { return height; }
        }

        public int radius
        {
            get { return _radius; }
        }
        #endregion;

        #region constructors
        public SomeFigure(int x, int y, int width, int height)
        {
            this._x = x;
            this._y = y;
            this._width = width;
            this._height = height;
        }

        public SomeFigure(int x, int y, int radius)
        {
            this._x = x;
            this._y = y;
            this._radius = radius;
        }
        #endregion;

        public virtual void Draw()
        {

        }


    }
}
