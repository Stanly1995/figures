﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FiguresLibrary
{
    public class Round: SomeFigure
    {
        #region properties
        public Round(int x, int y, int radius) : base(x, y, radius)
        {

        }
        #endregion;

        override public void Draw()
        {
            for (int i=1; i<=360; i++)
            {
                Console.SetCursorPosition((int)((radius)*Math.Cos(i))+1+x, (int)((((radius) * Math.Sin(i))+1+y)));
                Console.Write("*");
            }
        }

    }
}
